﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using OxyPlot;

namespace FbgWDM
{
    /// <summary>
    /// Главное окно 
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        /// <summary>
        /// Модель, хранящая в себе данные и команды управления процессами
        /// </summary>
        public MainWindowViewModel ViewModel
        {
            get { return DataContext as MainWindowViewModel; }
            set
            {
                var viewModel = ViewModel;
                if (viewModel != null)
                {
                    viewModel.AdjustPlotEvent -= AdjustPlotEventHandler;
                }
                DataContext = value;
                ViewModel.AdjustPlotEvent += AdjustPlotEventHandler;
            }
        }

        /// <summary>
        /// Главное окно 
        /// </summary>
        public MainWindow()
        {
            ViewModel = new MainWindowViewModel();
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик требования вернуть графику изначальный масштаб 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void AdjustPlotEventHandler(object sender, EventArgs args)
        {
            OxyPlot.ResetAllAxes();
        }
    }
}
