﻿namespace FbgWDM.Wolfram
{
    /// <summary>
    /// Результат вычислений в ядре Wolfram
    /// </summary>
    public class WolframICalculationResponse
    {
        /// <summary>
        /// Результат вычислений в ядре Wolfram
        /// </summary>
        public WolframICalculationResponse(FunctionValue[] functionValues, bool isSuccessfully = true, string errorMessage = null)
        {
            IsSuccessfully = isSuccessfully;
            ErrorMessage = errorMessage;
            FunctionValues = functionValues;
        }
        
        /// <summary>
        /// Признак успешного вычисления
        /// </summary>
        public bool IsSuccessfully { get; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; }

        /// <summary>
        /// Множество значений функции
        /// </summary>
        public FunctionValue[] FunctionValues { get; }
    }

}