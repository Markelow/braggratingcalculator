﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Wolfram.NETLink;

namespace FbgWDM.Wolfram
{
    /// <summary>
    /// Обертка вокруг Wolfram для вычисления решетки на основе введенных параметров
    /// </summary>
    public class BragGratingCalculator : IDisposable
    {
        /// <summary>
        /// Объект взаимодействия с ядром Wolfram
        /// </summary>
        private readonly MathKernel _mathKernel;

        private readonly List<Dictionary<double, double>> _tFactorsSet;

        /// <summary>
        /// Обертка вокруг Wolfram для вычисления решетки на основе введенных параметров
        /// </summary>
        public BragGratingCalculator()
        {
            // инициализация
             _mathKernel =
                new MathKernel
                {
                    AutoCloseLink = true,
                    CaptureGraphics = true,
                    CapturePrint = true,
                    ResultFormat = MathKernel.ResultFormatType.InputForm,
                    UseFrontEnd = true
                };
            _tFactorsSet = new List<Dictionary<double, double>>();
        }

        /// <summary>
        /// Асинхронно вычисляет значения решетки в указанном диапазоне
        /// </summary>
        /// <param name="l"></param>
        /// <param name="no"></param>
        /// <param name="lyambda"></param>
        /// <param name="s"></param>
        /// <param name="deltaN"></param>
        /// <param name="lMin"></param>
        /// <param name="lMax"></param>
        /// <returns></returns>
        public async Task<WolframICalculationResponse> CalculateAsync(
            double l,
            double no,
            double lyambda,
            double s,
            double deltaN, 
            double lMin,
            double lMax)
        {
            return await Task.Factory.StartNew(() =>
            {
                // очищаем ядро от предыдущих вычислений
                var mathKernel = _mathKernel;
                mathKernel.Clear();

                // преобразуем данные в формат Wolfram
                var lWolfram = WolframDoubleConverter.ConvertToWolframString(l);
                var noWolfram = WolframDoubleConverter.ConvertToWolframString(no);
                var lambdaWolfram = WolframDoubleConverter.ConvertToWolframString(lyambda);
                var sWolfram = WolframDoubleConverter.ConvertToWolframString(s);
                var deltaNWolfram = WolframDoubleConverter.ConvertToWolframString(deltaN);
                var minLyabmda = WolframDoubleConverter.ConvertToWolframString(lMin);
                var maxLyabmda = WolframDoubleConverter.ConvertToWolframString(lMax);

                // передаем ядру команды на вычисления
                mathKernel.Compute(
                    $"L = {lWolfram}; " +
                    $"no = {noWolfram}; " +
                    $"lambda = {lambdaWolfram}; " +
                    $"s = {sWolfram}; " +
                    "AtJ = 0.015*10^-9; " +
                    "lb = 2*no*lambda; " +
                    $"Ndc = {deltaNWolfram}; " +
                    $"lmin = 2*no*{minLyabmda} - 1*10^-9; " +
                    $"lmax = 2*no*{maxLyabmda} + 1*10^-9; ");


                mathKernel.Compute("k[lam_] := Pi*s*Ndc/lam;");

                mathKernel.Compute("zeta[lam_] := 2*Pi*no*(1/lam - 1/(lb - AtJ)) + 2*Pi*Ndc/lam;");

                mathKernel.Compute(
                    "R[lam_] :=  Power[Sinh[Sqrt[(k[lam] * L) ^ 2 - (zeta[lam] * L) ^ 2]], 2] / " +
                    "(Power[Cosh[Sqrt[(k[lam] * L) ^ 2 - (zeta[lam] * L) ^ 2]], 2] - (zeta[lam] / k[lam]) ^ 2); ");

                // без этого почему-то не работает
                mathKernel.GraphicsHeight = 500;
                mathKernel.GraphicsWidth = 500;

                mathKernel.Compute("Table[{Abs[R[lam]], lam}, { lam, lmin, lmax, 10 ^ -12} ]");

                // здесь должны получить конечный результат, если его нет, то возвращаем ошибку
                if (mathKernel.Result != null)
                {
                    var values = ConvertToValues(mathKernel.Result as string);
                    UpdateTFactors(values);
                    values = RecalculateWithTFactors(values);

                    return new WolframICalculationResponse(values);
                }
                return new WolframICalculationResponse(null, false, mathKernel.Messages.FirstOrDefault());    
            });


        }

        /// <summary>
        /// Преобразует конечный результат вычисления Wolfram в множество пар аргумент-значения функции
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private FunctionValue[] ConvertToValues(string text)
        {
            if (String.IsNullOrEmpty(text)) return new FunctionValue[0];

            var values = new List<FunctionValue>();

            // удалим первую и последнюю скобку, т.к. данные окружены скобками
            var temp = text.Remove(0, 1).Remove(text.Length - 2, 1);

            // с помощью регулярных выражения извлекаем строки и отдельные значения в строках
            //NOTE в ПЗ можно включить формат этого регулярного выражения
            var pattern = @"{([\d.*^Ee-]+), ([\d.*^Ee-]+)}";

            var regex = new Regex(pattern);

            var matches = regex.Matches(text);
            foreach (Match match in matches)
            {
                if (match.Groups.Count < 3) continue;

                var value = match.Groups[2].Value;
                var function = match.Groups[1].Value;

                values.Add(
                    new FunctionValue(
                        WolframDoubleConverter.ConvertFromWolframString(value), 
                        WolframDoubleConverter.ConvertFromWolframString(function)));
            }

            return values.ToArray();
        }

        /// <summary>
        /// Вычисляет и запоминает T для текущей решетки
        /// </summary>
        /// <param name="values"></param>
        private void UpdateTFactors(FunctionValue[] values)
        {
            var tFactors = new Dictionary<double, double>();
            foreach (var functionValue in values)
            {
                tFactors.Add(functionValue.Argument, 1-functionValue.Result);
            }
            _tFactorsSet.Add(tFactors);
        }

        /// <summary>
        /// Корректирует значения решетки с учетом предыдущих T, 
        /// </summary>
        /// <param name="values">Расчитанные значения решетки</param>
        /// <returns>Скорректированые значения решетки</returns>
        private FunctionValue[] RecalculateWithTFactors(FunctionValue[] values)
        {
            var calculatedValues = new FunctionValue[values.Length];

            var counter = 0;
            foreach (var functionValue in values)
            {
                var result = functionValue.Result;
                for (var i = 0; i < _tFactorsSet.Count -1; i++)
                {
                    result *= _tFactorsSet[i][functionValue.Argument];
                }

                calculatedValues[counter++] = new FunctionValue(functionValue.Argument, result);
            }

            return calculatedValues;
        }

        /// <summary>
        /// Освобождает калькулятор, отсоединяется от ядра Wolfram и освобождает занятые им ресурсы
        /// </summary>
        public void Dispose()
        {
            _mathKernel.Clear();
            _mathKernel.AutoCloseLink = true;
            _mathKernel.Dispose();
        }
    }
}