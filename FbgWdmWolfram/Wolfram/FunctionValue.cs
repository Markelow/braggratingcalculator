﻿namespace FbgWDM.Wolfram
{
    /// <summary>
    /// Значение функции
    /// </summary>
    public class FunctionValue
    {
        /// <summary>
        /// Значение функции
        /// </summary>
        public FunctionValue(double argument, double result)
        {
            Argument = argument;
            Result = result;
        }

        /// <summary>
        /// Аргумент функции
        /// </summary>
        public double Argument { get; }

        /// <summary>
        /// Значение функции при заданном аргументе
        /// </summary>
        public double Result { get; }
    }
}