﻿using System;
using System.Drawing;

namespace FbgWDM.Wolfram
{
    /// <summary>
    /// Ответ ядра Wolfram, содержащий изображение
    /// </summary>
    /// <remarks>
    /// Сейчас не исползуется, перешли на вычисления без изображений в качестве посредникков
    /// </remarks>
    [Obsolete]
    public class WolframImageResponse
    {
        /// <summary>
        /// Ответ ядра Wolfram, содержащий изображение
        /// </summary>
        public WolframImageResponse(Image image, bool isSuccessfully = true, string errorMessage = null)
        {
            Image = image;
            IsSuccessfully = isSuccessfully;
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Изображение
        /// </summary>
        public Image Image { get; }

        /// <summary>
        /// Признак успешной операции
        /// </summary>
        public bool IsSuccessfully { get; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; }
    }
}