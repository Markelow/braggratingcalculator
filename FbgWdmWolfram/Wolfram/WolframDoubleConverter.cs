﻿using System.Globalization;

namespace FbgWDM.Wolfram
{
    /// <summary>
    /// Конвертер чисел из формата .Net в формат языка Wolfram
    /// </summary>
    public class WolframDoubleConverter
    {
        /// <summary>
        /// Преобразует дробное число в строковое представление дробного числа в Wolfram
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Строковое представление числа в Wolfram</returns>
        public static string ConvertToWolframString(double value)
        {
            var result = value.ToString(CultureInfo.InvariantCulture).Replace(",", ".");
            return result.Replace("E", "*10^");
        }

        /// <summary>
        /// Преобразует строковое представление дробного числа в Wolfram в дробное число .Net
        /// </summary>
        /// <param name="value">Строковое представление дробного числа в Wolfram </param>
        /// <returns>Дробное число .Net</returns>
        public static double ConvertFromWolframString(string value)
        {
            var temp = value.Replace("*10^", "E");
            temp = temp.Replace("*^", "E").Replace(".", ","); ;

            var result = 0.0;

            double.TryParse(temp, out result);

            return result;
        }
    }
}