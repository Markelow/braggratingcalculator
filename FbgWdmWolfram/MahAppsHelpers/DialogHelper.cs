﻿using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace FbgWDM.MahAppsHelpers
{
    /// <summary>
    /// Вспомогательный класс для асинхронного отображения диалоговых оконо MahApps
    /// </summary>
    public class DialogHelper
    {
        /// <summary>
        /// Отображает указанное сообщение в диалоговом окне
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="title">Заголовок</param>
        /// <param name="dialogStyle">Кнопки, отображаемые в окне</param>
        /// <returns>Результат диалога</returns>
        public static async Task<MessageDialogResult> ShowMessage(string message, string title, MessageDialogStyle dialogStyle)
        {
            // ищем открытое окно MahApss
            var metroWindow = Application.Current.MainWindow as MetroWindow;
            if (metroWindow == null) return MessageDialogResult.Negative;

            // задаем схему
            metroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented;
             
            // асинхронно отображаем диаолговое окно
            return await metroWindow.ShowMessageAsync(title, message, dialogStyle, metroWindow.MetroDialogOptions);
        }
    }
}