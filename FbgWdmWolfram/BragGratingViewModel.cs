﻿using System;
using System.ComponentModel;
using System.Globalization;
using TinyLittleMvvm;

namespace FbgWDM
{
    /// <summary>
    /// Модель Брэгговской решетки, используемая для отображения и получения данных
    /// </summary>
    public class BragGratingViewModel : PropertyChangedBase, IDataErrorInfo
    {
        #region Поля

        private string _lParam;
        private string _n0Param;
        private string _lyambdaParam;
        private string _sParam;
        private string _deltaNParam;

        private int _gratingNumber;

        #endregion

        /// <summary>
        /// Модель Брэгговской решетки, используемая для отображения и получения данных
        /// </summary>
        public BragGratingViewModel()
        {
            LParam = "14E-3";
            N0Param = "1.45452";
            LyambdaParam = "5.328E-7";
            SParam = "0.94";
            DeltaNParam = "0.000017139479567929025";
            GratingNumber = 0;

            RemoveBragGattingCommand = new RelayCommand(RemoveBragGattingExecute, CanRemoveBragGatting);
        }

        #region Свойства

        public string LParam
        {
            get { return _lParam; }
            set
            {
                _lParam = value;
                NotifyOfPropertyChange(nameof(LParam));
            }
        }

        public string N0Param
        {
            get { return _n0Param; }
            set
            {
                _n0Param = value;
                NotifyOfPropertyChange(nameof(N0Param));
            }
        }

        public string SParam
        {
            get { return _sParam; }
            set
            {
                _sParam = value;
                NotifyOfPropertyChange(nameof(SParam));
            }
        }

        public string LyambdaParam
        {
            get { return _lyambdaParam; }
            set
            {
                _lyambdaParam = value;
                NotifyOfPropertyChange(nameof(LyambdaParam));
            }
        }

        public string DeltaNParam
        {
            get { return _deltaNParam; }
            set
            {
                _deltaNParam = value;
                NotifyOfPropertyChange(nameof(DeltaNParam));
            }
        }

        /// <summary>
        /// Порядковый номер решетки в списке
        /// </summary>
        public int GratingNumber
        {
            get { return _gratingNumber; }
            set
            {
                _gratingNumber = value;
                NotifyOfPropertyChange(nameof(GratingNumber));
                NotifyOfPropertyChange(nameof(Header));
            }
        }
        
        public double L
        {
            get
            {
                double temp;
                DoubleTryParse(LParam, out temp);
                return temp;
            }
        }

        public double No
        {
            get
            {
                double temp;
                DoubleTryParse(N0Param, out temp);
                return temp;
            }
        }

        public double S
        {
            get
            {
                double temp;
                DoubleTryParse(SParam, out temp);
                return temp;
            }
        }

        public double Laymbda
        {
            get
            {
                double temp;
                DoubleTryParse(LyambdaParam, out temp);
                return temp;
            }
        }

        public double DeltaN
        {
            get
            {
                double temp;
                DoubleTryParse(DeltaNParam, out temp);
                return temp;
            }
        }

        /// <summary>
        /// Заголовок, содержащий номер решетки в списке
        /// </summary>
        public string Header
        {
            get { return GratingNumber > 0 
                    ? $"Брэгговскя решета № {GratingNumber}"
                    : "Брэгговскя решета"; }
        }

        #endregion

        /// <summary>
        /// Метод, преобразующий строку в число с плавающей запятой
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool DoubleTryParse(string value, out double result)
        {
            //Try parsing in the current culture
            if (!double.TryParse(value, NumberStyles.Any, CultureInfo.CurrentCulture, out result) &&
                //Then try in US english
                !double.TryParse(value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out result) &&
                //Then in neutral language
                !double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return  false;
            }

            return true;
        }

        #region События

        /// <summary>
        /// Событие удаления этой решетки из списка
        /// </summary>
        public event EventHandler OnRemoved;

        #endregion

        #region Команды

        /// <summary>
        /// Команда удаления решетки из списка
        /// </summary>
        public RelayCommand RemoveBragGattingCommand { get; }

        /// <summary>
        /// Метод, выполняющий команду удаления решетки из списка
        /// </summary>
        private void RemoveBragGattingExecute()
        {
            var handler = OnRemoved;
            handler?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Метод, разрежающий выполнять команду удаления решетки из списка
        /// </summary>
        private bool CanRemoveBragGatting()
        {
            return true;
        }


        #endregion

        #region Валидация

        /// <summary>
        /// Проверяет корректность введенных данных
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public string this[string columnName]
        {
            get
            {
                double temp;
                if (String.Equals(columnName, nameof(LParam)) || String.IsNullOrEmpty(columnName))
                {
                    if (!DoubleTryParse(LParam, out temp))
                    {
                        return "Неверный формат дробного числа";
                    }
                }
                if (String.Equals(columnName, nameof(N0Param)) || String.IsNullOrEmpty(columnName))
                {
                    if (!DoubleTryParse(N0Param, out temp))
                    {
                        return "Неверный формат дробного числа";
                    }
                }
                if (String.Equals(columnName, nameof(SParam)) || String.IsNullOrEmpty(columnName))
                {
                    if (!DoubleTryParse(SParam, out temp))
                    {
                        return "Неверный формат дробного числа";
                    }
                }
                if (String.Equals(columnName, nameof(LyambdaParam)) || String.IsNullOrEmpty(columnName))
                {
                    if (!DoubleTryParse(LyambdaParam, out temp))
                    {
                        return "Неверный формат дробного числа";
                    }
                }
                if (String.Equals(columnName, nameof(DeltaNParam)) || String.IsNullOrEmpty(columnName))
                {
                    if (!DoubleTryParse(DeltaNParam, out temp))
                    {
                        return "Неверный формат дробного числа";
                    }
                }
                return null;
            }
        }

        public string Error
        {
            get { return String.Empty; }
        }

        /// <summary>
        /// Возвращает признак корректно заполненной модели
        /// </summary>
        public bool IsValid
        {
            get { return String.IsNullOrEmpty(this[String.Empty]); }
        }

        #endregion
    }
}