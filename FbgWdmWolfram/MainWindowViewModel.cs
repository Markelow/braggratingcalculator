﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using FbgWDM.MahAppsHelpers;
using FbgWDM.Wolfram;
using MahApps.Metro;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Wpf;
using TinyLittleMvvm;
using Color = System.Windows.Media.Color;
using LinearAxis = OxyPlot.Axes.LinearAxis;
using LineSeries = OxyPlot.Series.LineSeries;

namespace FbgWDM
{
    /// <summary>
    /// Модель с данными и командами для отображения всего
    /// </summary>
    public class MainWindowViewModel : PropertyChangedBase
    {
        #region Константы

        private const int DefaultPlotExportWidth = 1024;
        private const int DefaultPlotExportHeight = 768;
        private static readonly OxyColor PlotExportBackground = OxyColors.White;

        #endregion

        #region Поля

        private PlotModel _plotModel;

        private bool _isPlotDrawing;

        private ObservableCollection<BragGratingViewModel> _bragGratings;
        private IReadOnlyList<DataPoint> _points;

        private string _gatingErrorText;
        
        private int _plotExportWidth;
        private int _plotExportHeight;

        #endregion

        /// <summary>
        /// Модель с данными и командами для отображения всего
        /// </summary>
        public MainWindowViewModel()
        {
            // инициализация команд
            DrawPlotCommand = new RelayCommand(DrawPlotExecute, CanDrawPlot);
            AddBragGratingCommand = new RelayCommand(AddBragGratingExecute, CanAddBragGrating);
            SavePointsCommand = new RelayCommand(SavePointsExecute, CanSavePoints);
            SavePlotCommand = new RelayCommand(SavePlotExecute, CanSavePlot);
            SavePlotToClipboardCommand = new RelayCommand(SavePlotToClipboardExecute, CanSavePlotToClipboard);
            AutoScaleCommand = new RelayCommand(AutoScaleExecute, CanAutoScale);

            // добавим в список одну решетку
            var bragGratingViewModel = new BragGratingViewModel();
            bragGratingViewModel.OnRemoved += BragGratingRemovedHandler;
            bragGratingViewModel.PropertyChanged += BragGratingViewModelPropertyChangedHandler;
            BragGratings = new ObservableCollection<BragGratingViewModel>
            {
                bragGratingViewModel
            };
            Points = new List<DataPoint>();

            // начальные настройки для экспорта
            PlotExportWidth = DefaultPlotExportWidth;
            PlotExportHeight = DefaultPlotExportHeight;

            // начальные параметры для автомасштабирования
            MinX = double.MaxValue;
            MinY = double.MaxValue;

            MaxX = double.MinValue;
            MaxY = double.MinValue;
        }

        #region Свойства с уведомлениями представления (данные в UI автоматически уведомляются)

        /// <summary>
        /// Модель с данными для графика
        /// </summary>
        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set
            {
                _plotModel = value;
                NotifyOfPropertyChange(nameof(PlotModel));
            }
        }
        
        /// <summary>
        /// Выполняется ли сейчас процесс построения графика
        /// </summary>
        public bool IsPlotDrawing
        {
            get { return _isPlotDrawing; }
            set
            {
                _isPlotDrawing = value;
                NotifyOfPropertyChange(nameof(IsPlotDrawing));
                DrawPlotCommand.RaiseCanExecuteChanged();
            }
        }
        
        /// <summary>
        /// Список Брэгговских решеток
        /// </summary>
        public ObservableCollection<BragGratingViewModel> BragGratings
        {
            get { return _bragGratings; }
            private set
            {
                _bragGratings = value;

                NotifyOfPropertyChange(nameof(BragGratings));
                DrawPlotCommand.RaiseCanExecuteChanged();
            }
        }
        
        /// <summary>
        /// Список точек, значения которых вычислены для заданных решеток
        /// </summary>
        public IReadOnlyList<DataPoint> Points
        {
            get { return _points; }
            private set
            {
                _points = value;
                NotifyOfPropertyChange(nameof(Points));
                SavePointsCommand?.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Сообщений об ошибке в параметрах решеток
        /// </summary>
        public string GatingErrorText
        {
            get { return _gatingErrorText; }
            set
            {
                _gatingErrorText = value;
                NotifyOfPropertyChange(nameof(GatingErrorText));
                NotifyOfPropertyChange(nameof(ErrorTextVisibility));
            }
        }

        /// <summary>
        /// Признак видимости сообщения об ошибке
        /// </summary>
        public Visibility ErrorTextVisibility
        {
            get { return String.IsNullOrEmpty(GatingErrorText) ? Visibility.Collapsed : Visibility.Visible; }
        }

        /// <summary>
        /// Ширина экспортируемого изображения
        /// </summary>
        public int PlotExportWidth
        {
            get { return _plotExportWidth; }
            set
            {
                _plotExportWidth = value;
                NotifyOfPropertyChange(nameof(PlotExportWidth));
            }
        }

        /// <summary>
        /// Высота экспортируемого изображения
        /// </summary>
        public int PlotExportHeight
        {
            get { return _plotExportHeight; }
            set
            {
                _plotExportHeight = value;
                NotifyOfPropertyChange(nameof(PlotExportHeight));
            }
        }

        #endregion

        #region Свойства

        /// <summary>
        /// X самой левой точки
        /// </summary>
        public double MinX { get; private set; }

        /// <summary>
        /// X самой правой точки
        /// </summary>
        public double MaxX { get; private set; }

        /// <summary>
        /// У самой "низкой" точки
        /// </summary>
        public double MinY { get; private set; }

        /// <summary>
        /// У самой "высокой" точки
        /// </summary>
        public double MaxY { get; private set; }

        #endregion

        #region Команды

        /// <summary>
        /// Команда построения графика
        /// </summary>
        public RelayCommand DrawPlotCommand { get; }

        private void DrawPlotExecute()
        {
            DrawBragGrating();
        }

        private bool CanDrawPlot()
        {
            if (IsPlotDrawing) return false;
            if (BragGratings == null || BragGratings.Count == 0) return false;

            foreach (var bragGratingViewModel in BragGratings)
            {
                if (!bragGratingViewModel.IsValid)
                {
                    GatingErrorText = "В параметрах решеток есть ошибки";
                    return false;
                }
            }
            GatingErrorText = String.Empty;
            return true;
        }

        /// <summary>
        /// Команда добавления новой решетки в список
        /// </summary>
        public RelayCommand AddBragGratingCommand { get; }
        
        private void AddBragGratingExecute()
        {
            var bragGratingViewModel = new BragGratingViewModel();
            bragGratingViewModel.OnRemoved += BragGratingRemovedHandler;
            bragGratingViewModel.PropertyChanged += BragGratingViewModelPropertyChangedHandler;
            BragGratings.Add(bragGratingViewModel);

            RecalculateNumbers();
        }

        private bool CanAddBragGrating()
        {
            return true;
        }

        /// <summary>
        /// Команда сохранения точек в файл
        /// </summary>
        public RelayCommand SavePointsCommand { get; }

        private void SavePointsExecute()
        {
            SavePoints();
        }

        private bool CanSavePoints()
        {
            return Points != null && Points.Count > 0;
        }

        /// <summary>
        /// Команда экспора графика в файл
        /// </summary>
        public RelayCommand SavePlotCommand { get; }

        private void SavePlotExecute()
        {
            SavePlot();
        }

        private bool CanSavePlot()
        {
            return PlotModel?.Series != null && PlotModel.Series.Count > 0;
        }

        /// <summary>
        /// Команда сохранения графика в буффер обмена
        /// </summary>
        public RelayCommand SavePlotToClipboardCommand { get; }

        private void SavePlotToClipboardExecute()
        {
            SavePlotToClipboard();
        }

        private bool CanSavePlotToClipboard()
        {
            return PlotModel?.Series != null && PlotModel.Series.Count > 0;
        }

        /// <summary>
        /// Команда возврата масштаба к начальному
        /// </summary>
        public RelayCommand AutoScaleCommand { get; }

        private void AutoScaleExecute()
        {
            RaiseAdjustPlotEvent();
        }

        private bool CanAutoScale()
        {
            return PlotModel?.Series != null && PlotModel.Series.Count > 0;
        }
        #endregion

        #region События

        /// <summary>
        /// Событие, вызываемое при возврате к масштаба к начальному уровню
        /// </summary>
        public EventHandler AdjustPlotEvent { get; set; }

        #endregion

        #region Обработчики событий

        /// <summary>
        /// Обработчик события изменения свойства в модели Брэгговской решетки
        /// </summary>
        /// <param name="sender">Модель решетки</param>
        /// <param name="args">Аргументы события</param>
        private void BragGratingViewModelPropertyChangedHandler(object sender, EventArgs args)
        {
            DrawPlotCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Обработчик события удаления Брэгговской решетки из списка
        /// </summary>
        /// <param name="sender">Удаляемая решетка</param>
        /// <param name="args">Аргументы события</param>
        private void BragGratingRemovedHandler(object sender, EventArgs args)
        {
            var grating = sender as BragGratingViewModel;
            if (grating == null) return;

            grating.OnRemoved -= BragGratingRemovedHandler;
            grating.PropertyChanged -= BragGratingViewModelPropertyChangedHandler;
            BragGratings.Remove(grating);
            RecalculateNumbers();
            DrawPlotCommand?.RaiseCanExecuteChanged();
        }
        
        #endregion

        #region Подготовки данных и их представлений

        /// <summary>
        /// Пересчитывает порядковые номера решеток после операции добавления или удаления
        /// </summary>
        private void RecalculateNumbers()
        {
            if (BragGratings == null || BragGratings.Count == 0) return;

            var number = 1;
            if (BragGratings.Count == 1)
            {
                var bragGratingViewModel = BragGratings.FirstOrDefault();
                if (bragGratingViewModel == null) return;
                bragGratingViewModel.GratingNumber = 0;
                return;
            }

            foreach (var bragGratingViewModel in BragGratings)
            {
                if (bragGratingViewModel == null) continue;
                bragGratingViewModel.GratingNumber = number++;
            }
        }
        
        #endregion
        
        #region Построение решеток и их отображение

        /// <summary>
        /// Отрисовывает Брэгговские решетки на графике
        /// </summary>
        private async void DrawBragGrating()
        {
            IsPlotDrawing = true;

            try
            {

                var errors = new StringBuilder();

                var bragGratings = BragGratings;
                var points = new Dictionary<double, double>();
                //определяем диапазон lyambda, чтобы считать для всех решеток в одинаковом диапазоне
                var lyabmdaMin = double.MaxValue;
                var lyabmdaMax = double.MinValue;
                foreach (var bragGrating in bragGratings)
                {
                    lyabmdaMax = Math.Max(lyabmdaMax, bragGrating.Laymbda);
                    lyabmdaMin = Math.Min(lyabmdaMin, bragGrating.Laymbda);
                }

                using (var calculator = new BragGratingCalculator())
                { 
                    // считаем значения каждой решетки в заданном диапазоне
                    foreach (var bragGrating in bragGratings)
                    {
                        if (bragGrating == null || !bragGrating.IsValid) continue;

                        var response = await calculator.CalculateAsync(
                            bragGrating.L,
                            bragGrating.No,
                            bragGrating.Laymbda,
                            bragGrating.S,
                            bragGrating.DeltaN,
                            lyabmdaMin,
                            lyabmdaMax).ConfigureAwait(true);

                        if (response.IsSuccessfully && response.FunctionValues != null)
                        {
                            foreach (var functionValue in response.FunctionValues)
                            {
                                // сумируем значения в одной точке
                                if (points.ContainsKey(functionValue.Argument))
                                {
                                    points[functionValue.Argument] += functionValue.Result;
                                }
                                else
                                {
                                    points.Add(functionValue.Argument, functionValue.Result);
                                }
                            }
                        }
                        else
                        {
                            errors.AppendLine(response.ErrorMessage);
                        }
                    }
                }
                var pointsList = new List<DataPoint>();
                foreach (var point in points)
                {
                    UpdateMinAndMaxValues(point.Key, point.Value);
                    pointsList.Add(new DataPoint(point.Key, point.Value));
                }

                // сохраняем точки для дальнейших операций, отрисовываем график
                Points = new List<DataPoint>(pointsList);
                DrawPlot(pointsList);

                if (errors.Length > 0)
                {
                    await DialogHelper.ShowMessage(errors.ToString(), "Ошибка", MessageDialogStyle.Affirmative).ConfigureAwait(true);
                }
            }
            catch (Exception ex)
            {
                await DialogHelper.ShowMessage(ex.Message, "Ошибка", MessageDialogStyle.Affirmative).ConfigureAwait(true);
            }
            finally
            {
                IsPlotDrawing = false;
            }
        }

        /// <summary>
        /// Обновляет крайние координаты
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <remarks>Необходимо для автомасштабирования</remarks>
        private void UpdateMinAndMaxValues(double x, double y)
        {
            MinY = Math.Min(MinY, y);
            MinX = Math.Min(MinX, x);
            MaxY = Math.Max(MaxY, y);
            MaxX = Math.Max(MaxX, x);
        }

        /// <summary>
        /// Отрисовывает точки на графике
        /// </summary>
        /// <param name="points">Список точек</param>
        private void DrawPlot(List<DataPoint> points)
        {
            if (points == null) throw new ArgumentNullException(nameof(points));

            var model = new PlotModel();

            model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom });
            model.Axes.Add(new LinearAxis { Position = AxisPosition.Left });

            var linearSeries = new LineSeries();
            linearSeries.Points.AddRange(points);

            var currentTheme = ThemeManager.DetectAppStyle(Application.Current);
            var accentColor = currentTheme.Item2;
            var plotColor = accentColor.Resources["AccentColor"] as Color? ?? Colors.Blue;

            linearSeries.Color = OxyColor.FromRgb(plotColor.R, plotColor.G, plotColor.B);
            model.Series.Add(linearSeries);
            PlotModel = model;
            RaiseAdjustPlotEvent();
        }

        /// <summary>
        /// Вызывает событие возврата масштаба к начальному
        /// </summary>
        private void RaiseAdjustPlotEvent()
        {
            var handler = AdjustPlotEvent;

            handler?.Invoke(this, EventArgs.Empty);
        }

        #endregion

        #region Сохранение результатов

        /// <summary>
        /// Сохраняет точки в файл
        /// </summary>
        private void SavePoints()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "CSV файл| *.csv",
                FileName = "BragGratingResult.csv"
            };
            
            if (saveFileDialog.ShowDialog() == true)
            {
                SavePointsToFileAsync(saveFileDialog.FileName);
            }
        }

        /// <summary>
        /// Сохраняет точки в указанный файл
        /// </summary>
        private async void SavePointsToFileAsync(string fileName)
        {
            try
            {
                using (var outputFile = new StreamWriter(fileName, false))
                {
                    foreach (var dataPoint in Points)
                    {
                        await outputFile.WriteLineAsync($"{dataPoint.X};{dataPoint.Y}").ConfigureAwait(true);
                    }
                }
                await DialogHelper.ShowMessage("Сохраненине завершено", "Сохранение", MessageDialogStyle.Affirmative).ConfigureAwait(true);
            }
            catch (Exception ex)
            {
                await DialogHelper.ShowMessage(ex.Message, "Ошибка", MessageDialogStyle.Affirmative).ConfigureAwait(true);
            }
        }

        /// <summary>
        /// Сохраняет график в файл
        /// </summary>
        private void SavePlot()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "png| *.png",
                FileName = "BragGratingPlot.png"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                SavePlotToFile(saveFileDialog.FileName);
            }
        }

        /// <summary>
        /// Сохраняет график в указанный файл
        /// </summary>
        private async void SavePlotToFile(string fileName)
        {
            try
            {
                var pngExporter = new PngExporter
                {
                    Width = PlotExportWidth,
                    Height = PlotExportHeight,
                    Background = PlotExportBackground
                };
                pngExporter.ExportToFile(PlotModel, fileName);
                await DialogHelper.ShowMessage("Сохранение графика выполнено", "Сохранение", MessageDialogStyle.Affirmative);
            }
            catch (Exception ex)
            {
                await DialogHelper.ShowMessage(ex.Message, "Ошибка", MessageDialogStyle.Affirmative);
            }
        }

        /// <summary>
        /// Сохраняет график в буффер обмена
        /// </summary>
        private async void SavePlotToClipboard()
        {
            try
            {
                var pngExporter = new PngExporter
                {
                    Width = PlotExportWidth,
                    Height = PlotExportHeight,
                    Background = PlotExportBackground
                };
                var bitmap = pngExporter.ExportToBitmap(PlotModel);
                Clipboard.SetImage(bitmap);
                await DialogHelper.ShowMessage("Копирование графика в буфер выполнено", "Сохранение", MessageDialogStyle.Affirmative);
            }
            catch (Exception ex)
            {
                await DialogHelper.ShowMessage(ex.Message, "Ошибка", MessageDialogStyle.Affirmative);
            }
        }

        #endregion
    }
}